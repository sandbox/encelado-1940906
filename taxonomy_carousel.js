function taxonomy_carousel_description_controls(carousel) {

  /*jQuery('.jcarousel-control a').bind('click', function() {
      carousel.scroll(jQuery.jcarousel.intval(jQuery(this).text()));
      return false;
  });

  jQuery('.jcarousel-scroll select').bind('change', function() {
      carousel.options.scroll = jQuery.jcarousel.intval(this.options[this.selectedIndex].value);
      return false;
  });*/

  jQuery('.carousel-description-next').bind('click', function() {
      carousel.next();
      return false;
  });

  /*jQuery('#carousel-description-prev').bind('click', function() {
      carousel.prev();
      return false;
  });*/
}

jQuery(document).ready(function() {
  jQuery('#taxonomy-carousel-summary').show().jcarousel({
    wrap: 'circular',
    scroll: 4
  });
  jQuery('#taxonomy-carousel-description').show().jcarousel({
    wrap: 'circular',
    scroll: 1,
    start: Math.floor(Math.random() * jQuery('#taxonomy-carousel-description li').length) + 1, // random slide
    initCallback: taxonomy_carousel_description_controls,
    buttonNextHTML: null,
    buttonPrevHTML: null
  });
});
