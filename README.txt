Description

This module provides a jCarousel-styled block summaryzing by vocabulary any
tagged node.


Dependencies

- taxonomy (required)
- jquery_update (required, to support jCarousel plugin)
- taxonomy_image (optional, to associate images with tags)
- statistics (optional, to sort tagged nodes by popularity)
- i18ntaxonomy (optional, to translate terms)


Plugin installation

Download jCarousel 0.2.8 from http://sorgalla.com/jcarousel and extract files
and folders (at least lib and skins) in jcarousel folder.


Credits

Jan Sorgalla (http://sorgalla.com/jcarousel)
